"use strict";
jQuery(document).ready(function($) {
  $(document).ready(function(){


    //home slider
    $('.home-slider').slick({
      infinite: true,
      speed: 300,
      dots: true,
      slidesToShow: 1,
      adaptiveHeight: true,
      autoplay: true,
      autoplaySpeed: 2000
    });


    //notification slider
    $('.notif-slider').slick({
      infinite: true,
      slidesToShow: 3,
      slidesToScroll: 3,
      speed: 300,
      dots: true,
      adaptiveHeight: true,
      autoplay: false,
      autoplaySpeed: 2000
    });


  });
});
